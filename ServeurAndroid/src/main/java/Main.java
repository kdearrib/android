import io.vertx.core.Vertx;

/**
 * Created by kevin on 27/02/17.
 */
public class Main {

    public static void main(String[] args) {
        Server server = new Server();
        Vertx vertx = Vertx.vertx();

        vertx.deployVerticle(server);

    }
}

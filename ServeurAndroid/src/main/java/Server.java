import io.vertx.core.AbstractVerticle;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpServer;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.sql.SQLConnection;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Created by kevin on 27/02/17.
 */
public class Server extends AbstractVerticle {

    private static final String SIGNIN = "/api/signin";
    private static final String SIGNUP = "/api/signup";
    private static final String ALLDRAWINGS = "/api/getAllDraws";
    private static final String SUBMITRESPONSE = "/api/submitResponse";
    private static final String SUBMITDRAWING = "/api/submitDraw";

    private static final String K_USERNAME = "username";
    private static final String K_PASS = "hashpassword";
    private static final String K_LOGIN = "login";
    private static final String K_LON = "lon";
    private static final String K_LAT = "lat";
    private static final String K_TOKEN = "token";

    private JDBCClient client;

    private final Logger logger = Logger.getLogger("SERVEUR");

    @Override
    public void start() throws Exception {

        client = JDBCClient.createShared(vertx, new JsonObject()
                .put("url", "jdbc:sqlite:./database.db")
                .put("driver_class", "org.sqlite.JDBC")
                .put("max_pool_size", 30));

        client.getConnection(res -> {
            if(res.succeeded()) {
                logger.log(Level.INFO, "Connexion réussie");

            } else {
                logger.log(Level.INFO, "Connexion échouée ");
            }
        });

        HttpServer server = vertx.createHttpServer();

        Router router = Router.router(vertx);

        router.get(SIGNIN).handler(this::signin);
        router.get(SIGNUP).handler(this::signUp);

        router.get("/api/hello").handler(hello());

        router.get("/api/dropTableDraw").handler(dropTable());

        router.get("/api/createTableDraws").handler(createTableDraws());

        router.get("/api/dropTableUsers").handler(dropTableUsers());

        router.get("/api/createTableUsers").handler(createTableUsers());


//        /* Vérification du token */
//        router.get("/api/*").handler(r -> {
//            logger.log(Level.INFO, "Request for api detected : "  + r.request().host() + " - " + r.request().uri());
//
//            String token = r.request().getParam("token");
//
//            if(null == token) {
//                logger.log(Level.INFO, "No token present");
//                return;
//            } else {
//                logger.log(Level.INFO, "Token : " + token);
//
//                client.getConnection(res -> {
//                    if(res.succeeded()) {
//
//                        //SQLConnection result = res.result();
//
////                        result.query("SELECT username from users where id = " + token, res2 -> {
////
////
////                            String username = res2.result().getRows().get(0).getString("username");
////
//                            //r.put("user", username);
//                            r.next();
////                        });
//
//
//                    }
//                 });
//
//
//            }
//
//
//        });

        router.get(ALLDRAWINGS).handler(this::getDraws);
        router.get(SUBMITRESPONSE).handler(this::submitResponse);
        router.post(SUBMITDRAWING).handler(this::submitDraw);

        server.requestHandler(router::accept).listen(7777);

    }

    private Handler<RoutingContext> createTableUsers() {
        return r -> {

            client.getConnection(resA -> {

                if (resA.succeeded()) {

                    final SQLConnection connection = resA.result();

                    connection.execute("CREATE TABLE USERS (\n" +
                            "  id INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                            "  username VARCHAR(255),\n" +
                            "  hashpassword VARCHAR(255),\n" +
                            "  login VARCHAR(255)\n" +
                            ");", rA -> {

                        if(rA.succeeded()) {

                            JsonObject jsonObject = new JsonObject();
                            jsonObject.put("error", 0);

                            r.response()
                                    .putHeader("content-type", "application/json")
                                    .end(jsonObject.encodePrettily());

                            return;


                        } else {

                            JsonObject jsonObject = new JsonObject();
                            jsonObject.put("error", 0);
                            jsonObject.put("message", rA.cause().getLocalizedMessage());

                            r.response()
                                    .putHeader("content-type", "application/json")
                                    .end(jsonObject.encode());

                        }

                    });


                } else {

                }

            });

        };
    }

    private Handler<RoutingContext> dropTableUsers() {
        return r -> {

            client.getConnection(resA -> {

                if(resA.succeeded()) {

                    final SQLConnection connection = resA.result();

                    connection.execute("DROP TABLE USERS", rA -> {

                        if(rA.succeeded()) {

                            JsonObject jsonObject = new JsonObject();
                            jsonObject.put("error", 0);

                            r.response()
                                    .putHeader("content-type", "application/json")
                                    .end(jsonObject.encodePrettily());


                        } else {

                            JsonObject jsonObject = new JsonObject();
                            jsonObject.put("error", 0);
                            jsonObject.put("message", rA.cause().getLocalizedMessage());

                            r.response()
                                    .putHeader("content-type", "application/json")
                                    .end(jsonObject.encode());

                        }


                    });

                } else {

                    JsonObject jsonObject = new JsonObject();
                    jsonObject.put("error", 0);
                    jsonObject.put("message", resA.cause().getLocalizedMessage());

                    r.response()
                            .putHeader("content-type", "application/json")
                            .end(jsonObject.encodePrettily());

                }

            });

        };
    }

    private Handler<RoutingContext> createTableDraws() {
        return r -> {

            client.getConnection(resA -> {

                if (resA.succeeded()) {

                    final SQLConnection connection = resA.result();

                    connection.execute("CREATE TABLE DRAWS (\n" +
                            "  id INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                            "  json TEXT,\n" +
                            "  token VARCHAR(255),\n" +
                            "  lon VARCHAR(255),\n" +
                            "  lat VARCHAR(255),\n" +
                            "  date_envoi DATE\n" +
                            ");", rA -> {

                        if(rA.succeeded()) {

                            JsonObject jsonObject = new JsonObject();
                            jsonObject.put("error", 0);

                            r.response()
                                    .putHeader("content-type", "application/json")
                                    .end(jsonObject.encode());

                            return;


                        } else {

                            JsonObject jsonObject = new JsonObject();
                            jsonObject.put("error", 1);
                            jsonObject.put("message", rA.cause().getLocalizedMessage());

                            r.response()
                                    .putHeader("content-type", "application/json")
                                    .end(jsonObject.encodePrettily());

                        }

                    });


                } else {

                    JsonObject jsonObject = new JsonObject();
                    jsonObject.put("error", 1);
                    jsonObject.put("message", resA.cause().getLocalizedMessage());

                    r.response()
                            .putHeader("content-type", "application/json")
                            .end(jsonObject.encode());

                }

            });

        };
    }

    private Handler<RoutingContext> dropTable() {
        return r -> {

            client.getConnection(resA -> {

                if(resA.succeeded()) {

                    final SQLConnection connection = resA.result();

                    connection.execute("DROP TABLE DRAWS", rA -> {

                        if(rA.succeeded()) {

                            JsonObject jsonObject = new JsonObject();
                            jsonObject.put("error", 0);

                            r.response()
                                    .putHeader("content-type", "application/json")
                                    .end(jsonObject.encodePrettily());

                            return;


                        } else {

                            JsonObject jsonObject = new JsonObject();
                            jsonObject.put("error", 1);
                            jsonObject.put("message", rA.cause().getLocalizedMessage());

                            r.response()
                                    .putHeader("content-type", "application/json")
                                    .end(jsonObject.encode());

                        }


                    });

                } else {

                    JsonObject jsonObject = new JsonObject();
                    jsonObject.put("error", 0);
                    jsonObject.put("message", resA.cause().getLocalizedMessage());

                    r.response()
                            .putHeader("content-type", "application/json")
                            .end(jsonObject.encodePrettily());

                }

            });


        };
    }

    private Handler<RoutingContext> hello() {
        return r -> {
            logger.log(Level.INFO, "REQUETE HELLO");
            r.response().putHeader("content-type", "text/html").end("Hello my friend");
        };
    }

    private void signUp(RoutingContext routingContext) {

        final String username = routingContext.request().getParam(K_USERNAME);
        final String login = routingContext.request().getParam(K_LOGIN);
        final String hashPass = routingContext.request().getParam(K_PASS);

        client.getConnection(res -> {

            if(res.succeeded()) {

                final SQLConnection connection = res.result();


                connection.execute("INSERT INTO USERS (login, username, hashpassword) VALUES ('" + login + "', '" + username + "', '" + hashPass + "')", res2 -> {
                    
                    if(res2.failed()) {
                        JsonObject jsonObject = new JsonObject();
                        jsonObject.put("error", 1);
                        jsonObject.put("message", res2.cause().getLocalizedMessage());

                        routingContext.response()
                                .putHeader("content-type", "application/json")
                                .end(jsonObject.encode());


                    } else {

                        JsonObject jsonObject = new JsonObject();
                        jsonObject.put("error", 0);

                        routingContext.response()
                                .putHeader("content-type", "application/json")
                                .end(jsonObject.encode());


                    }

                });

            }

        });

    }

    private void signin(RoutingContext routingContext) {

        final String username = routingContext.request().getParam(K_USERNAME);
        final String login = routingContext.request().getParam(K_LOGIN);
        final String hashPass = routingContext.request().getParam(K_PASS);

        logger.log(Level.INFO, "INFOS RECUES : " + username + " " + login + " " + hashPass);

        client.getConnection(res -> {

            if(res.succeeded()) {

                final SQLConnection connection = res.result();

                connection.query("SELECT ID FROM USERS WHERE login='" + login + "' AND hashpassword='" + hashPass + "'", res3 -> {

                        if(res3.failed()) {
                            logger.log(Level.INFO, "Impossible de faire la requete pour se connecter " + res3.cause().getLocalizedMessage() );

                            JsonObject jsonObject = new JsonObject();
                            jsonObject.put("error", 1);
                            jsonObject.put("message", res3.cause().getLocalizedMessage());

                            routingContext.response()
                                    .putHeader("content-type", "application/json")
                                    .end(jsonObject.encode());

                        } else {

                            logger.log(Level.INFO, "Requete OK !");

                            JsonObject jsonObject = new JsonObject();

                            if(res3.result().getResults().isEmpty()) {

                                jsonObject.put("error", 1);

                            } else {

                                jsonObject.put("error", 0);

                            }

                            routingContext.response()
                                    .putHeader("content-type", "application/json")
                                    .end(jsonObject.encode());

                        }

                });

            } else {

                logger.log(Level.INFO, "PAS BON : " + res.cause().getLocalizedMessage());

                JsonObject jsonObject = new JsonObject();
                jsonObject.put("error", 1);
                jsonObject.put("message", res.cause().getLocalizedMessage());

                routingContext.response()
                        .putHeader("content-type", "application/json")
                        .end(jsonObject.encode());

            }

        });



    }

    private void getDraws(RoutingContext routingContext) {

        final String lat = routingContext.request().getParam(K_LAT);
        final String lon = routingContext.request().getParam(K_LON);
        final String token = routingContext.request().getParam(K_TOKEN);

        client.getConnection(r -> {

          if(r.succeeded()) {

              logger.log(Level.INFO, "GET DRAWS : Connexion réussie");

              r.result().query("SELECT id, json FROM DRAWS", res -> {

                  if(res.succeeded()) {

                      logger.log(Level.INFO, "GET DRAWS : Select réussi");

                      List<JsonArray> output = res.result().getResults();

                      logger.log(Level.INFO, "Résultat null : " + (output == null) );

                      if(output == null) {
                          JsonObject jsonObject = new JsonObject();
                          jsonObject.put("error", 1);
                          jsonObject.put("message", "Pas de résultats");

                          routingContext.response()
                                  .putHeader("content-type", "application/json")
                                  .end(jsonObject.encode());

                          return;
                      }

                      for (JsonArray anOutput : output) {

                          logger.log(Level.INFO, anOutput.encodePrettily());

                      }

                      JsonObject jsonObject = new JsonObject();
                      jsonObject.put("error", 0);
                      jsonObject.put("draws", output);

                      routingContext.response()
                              .putHeader("content-type", "application/json")
                              .end(jsonObject.encode());


                  } else {

                      logger.log(Level.INFO, "Select pas réussi");

                      JsonObject jsonObject = new JsonObject();
                      jsonObject.put("error", 1);
                      jsonObject.put("message", res.cause().getLocalizedMessage());

                      routingContext.response()
                              .putHeader("content-type", "application/json")
                              .end(Json.encodePrettily(jsonObject));



                  }

              });

          } else {

              JsonObject jsonObject = new JsonObject();
              jsonObject.put("error", 1);

              routingContext.response()
                      .putHeader("content-type", "application/json")
                      .end(jsonObject.encode());

          }

        });





    }

    private void submitResponse(RoutingContext routingContext) {






    }

    private void submitDraw(RoutingContext routingContext) {

        final String lat = routingContext.request().getParam(K_LAT);
        final String lon = routingContext.request().getParam(K_LON);
        final String token = routingContext.request().getParam(K_TOKEN);


        logger.log(Level.INFO, "Information de la requete : (" + lat + ", " + lon + ") de " + token);

        routingContext.request().bodyHandler(r -> {


            String s = r.toJsonObject().encodePrettily();

            //logger.log(Level.INFO, s);

            client.getConnection(res -> {

                if(res.succeeded()) {

                    final SQLConnection connection = res.result();

                    String req = "INSERT INTO DRAWS (json, token, lat, lon, date_envoi) VALUES ('" + s + "', '" + token + "',  '" + lat + "', '" + lon + "', datetime())";

                    logger.log(Level.INFO, req);

                    connection.execute(req, resB -> {

                        if(resB.succeeded()) {

                            JsonObject jsonObject = new JsonObject();
                            jsonObject.put("error", 0);

                            routingContext.response()
                                    .putHeader("content-type", "application/json")
                                    .end(jsonObject.encode());

                            logger.log(Level.INFO, "REQUETE INSERT IS OK");

                        } else {

                            JsonObject jsonObject = new JsonObject();
                            jsonObject.put("error", 1);
                            jsonObject.put("message", resB.cause().getLocalizedMessage());

                            routingContext.response()
                                    .putHeader("content-type", "application/json")
                                    .end(jsonObject.encode());

                            logger.log(Level.INFO, "REQUETE INSERT IS NOT OK : " + resB.cause().getLocalizedMessage());
                        }

                    });



                }


            });


        });

//        JsonArray bodyAsJsonArray = routingContext.getBodyAsJsonArray();
//
//        if(bodyAsJsonArray == null) {
//            logger.log(Level.INFO, "Body is null");
//        } else {
//            logger.log(Level.INFO, "SIZE : " + bodyAsJsonArray.size());
//        }
//
//        String bodyAsString = routingContext.getBodyAsString();
//        logger.log(Level.INFO, bodyAsString);


    }

    private static float distFrom(float lat1, float lng1, float lat2, float lng2) {
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(lat2-lat1);
        double dLng = Math.toRadians(lng2-lng1);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        float dist = (float) (earthRadius * c);

        return dist;
    }
}

# Installation

Liste des choses à faire pour travailler sur le REPO 

```
1) faire un clone du projet
2) une fois dl se mettre dans de dossier android
3) mettre de coté les sources
4) créer à nouveau le projet avec android studio
5) Application name : CommunityDrawing
6) Company Domain : cyberleaf.apps.com
7) Project Location : androidApplication et non CommunityDrawing
8) Remettre les sources à la place de src
9) Vous pouvez maintenant push et pull sur le src.
```

# Important / Consignes

Listes des consignes a respecter !!

```
1) Toujours faire les commit / push / pull par ligne de commande (ça tombe bien androidStudio possede un terminal)
2) Pour que le commit marche faire :  'git add androidApplication/app/src/'
3) Si vous copiez quelque chose d'autre : demander à Thomas ou Benjamin de le supprimer (ou faites le si vous êtes sûr de vous ! Sinon Thomas est sûr :p.)
4) Si vous devez toucher du code qui n'est pas a vous, ou qui est commun : laisser le code present en commentaire ! (exemple du manifest)
5) Toutes autres consignes seront ajoutées ici !
```

# Mots Clés pour commit
Liste des mots clé pour comprendre les commits
```
[ADD] - ajout d'un objet, d'une classe ou d'une fonctionnalité, ajout de code de manière générale
[UPD] - modification de code ou de fonctionnalités présente
[DEL] - suppression
[nom_package] - exemple [drawingfragment] : action qui ne concerne que le package mentionné
```

# Utilisateur / Membres du groupe
Liste des membres du groupe pour les commits, en fonction des noms possibles à l'affichage
```
be5nx2 - Benjamin Campomenosi
Antiquaire - Thomas Kieffer
 - Kevin De Arriba
 - Eric Diallo
```

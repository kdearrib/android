package com.apps.cyberleaf.communitydrawing.drawingfragment.fragment.drawingzone.drawingview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;

import com.apps.cyberleaf.communitydrawing.drawingfragment.model.ModelListener;
import com.apps.cyberleaf.communitydrawing.drawingfragment.model.ModelParameters;
import com.apps.cyberleaf.communitydrawing.drawingfragment.model.Point;

/**
 * View perso qui stock en mémoire un canvas pour le dessin
 * Ceci est dans un unique but optimisation, cela evite de recharger le model a chaque fois
 * on met a jour a en fonction des notifications du ModelListener
 */
public class DrawingView extends View implements ModelListener{


    private Canvas performCanvas = new Canvas(); // permet d ecrire dans le bitmap qui stoque les donnees
    private Bitmap bitmap; // le bitmap qui stoque les donnees | but purement optimal
    private Paint paint = new Paint(); // un unique paint, encore pour de l optimisation



    private Rect drawingZone = new Rect(); // rectange qui defini la zone de dessin

    private ModelParameters modelParameters; // permet de modifier le model, enfin la la taille du model
        // pas vraiment top pour le MVC ...


    public DrawingView(Context context) {
        super(context);
    }

    public DrawingView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DrawingView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    public void setModelParameters(ModelParameters modelParameters) {
        this.modelParameters = modelParameters;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        // Les inits permettent d'obtenir une zone de dessin propre et conforme a ce que l on voulait

        initLayout();
        initRectDrawingZone();
        if(bitmap != null)
            canvas.drawBitmap(bitmap,0,0,null);
        drawEdge(canvas);
    }

    /**
     * initalise la vues !
     * Calcule x la taille du plus petit cote du rectangle, et fais un carre de x par x
     * On obtient ainsi une zone de dessin carre
     */
    private void initLayout() {
        if(isInit) return;
        int size = getWidth() < getHeight() ? getWidth() : getHeight();
        //yLinearLayout.LayoutParams laoutParams = new LinearLayout.LayoutParams(size, size);
        //AbsListView.LayoutParams layoutParams = new AbsListView.LayoutParams(size, size);

        setLayoutParams(getCurrentLayoutParams(size,size));
        modelParameters.setSizeOfView(size,size);
    }

    protected ViewGroup.LayoutParams getCurrentLayoutParams(int width, int height){
       return  new LinearLayout.LayoutParams(width, height);
        //return   new AbsListView.LayoutParams(width, height);
    }

    /**
     *  Dessine le contour de la zone de dessin
     * @param canvas  le canvas sur lequel dessiner les contours
     */
    private void drawEdge(Canvas canvas) {

        Rect left = new Rect(drawingZone.left, drawingZone.top, drawingZone.left + 1, drawingZone.bottom);
        Rect right = new Rect(drawingZone.right - 1, drawingZone.top, drawingZone.right, drawingZone.bottom);
        Rect top = new Rect(drawingZone.left, drawingZone.top, drawingZone.right, drawingZone.top + 1);
        Rect bottom = new Rect(drawingZone.left, drawingZone.top + getHeight() -1, drawingZone.right, drawingZone.top + getHeight());

        paint.setColor(Color.BLACK);
        canvas.drawRect(left, paint);
        canvas.drawRect(right, paint);
        canvas.drawRect(top, paint);
        canvas.drawRect(bottom, paint);
    }




    private boolean isInit = false;
    public void initRectDrawingZone() {

        if (isInit) return;
        drawingZone.set(0, 0, getWidth(), getHeight());
        isInit = true;
    }


    protected int getStrokeWidth(){
        return 15;
    }


    /**
     * Ajout dans le canvas d un point a la ligne courante
     * @param lastpoint le dernier point dessine de la ligne
     * @param newPoint le nouveau point du dessin a ajouter a la ligne
     * @param color la couleur de la ligne
     */
    @Override
    public void onAddToLine(Point lastpoint, Point newPoint, int color) {
        if(bitmap == null){
            initBitmap();
        }
        paint.setStrokeWidth(getStrokeWidth());
        this.paint.setColor(color);
        //drawcircle juste pour avoir un pseudo de ligne continue*

        if (getStrokeWidth() > 5) {
            performCanvas.drawCircle(lastpoint.getX() , lastpoint.getY(), 8,this.paint);
            performCanvas.drawCircle(newPoint.getX() , newPoint.getY() , 8,this.paint);
        }

        performCanvas.drawLine(lastpoint.getX(),lastpoint.getY(),
                newPoint.getX(),newPoint.getY(),paint);

        postInvalidate();
    }


    /**
     * Ajout d un point dans le canvas, avec la couleur qui definira la nouvelle ligne
     * @param startPoint le point a dessiner
     * @param color la couleur du point
     */
    @Override
    public void onNewLine(Point startPoint, int color) {
        if(bitmap == null){
            initBitmap();
        }

       // paint.setStrokeWidth(15);
        paint.setStrokeWidth(getStrokeWidth());
        this.paint.setColor(color);
        if(getStrokeWidth() > 5)
            performCanvas.drawCircle(startPoint.getX() , startPoint.getY() , 8,this.paint);

        postInvalidate();
    }

    @Override
    public void onSetCurrentWord(String currentWord) {
        //Rien a faire car on attends rien ici
    }

    /**
     * On efface le model, ici on efface le bitmap
     */
    @Override
    public void onClear() {
        initBitmap();
        this.postInvalidate();
    }

    /**
     * Initialise le bitmap pour la zone de dessins
     */
    private void initBitmap() {
        Bitmap.Config config = Bitmap.Config.ARGB_4444;
        this.bitmap = Bitmap.createBitmap(this.getWidth(),this.getHeight(),config );
        this.performCanvas .setBitmap(bitmap);
    }




}

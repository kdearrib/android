package com.apps.cyberleaf.communitydrawing.drawingfragment.fragment.parameterzone;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;

import com.apps.cyberleaf.communitydrawing.R;
import com.apps.cyberleaf.communitydrawing.drawingfragment.DrawingMainActivity;
import com.apps.cyberleaf.communitydrawing.drawingfragment.fragment.parameterzone.listcolorview.ColorListView;
import com.apps.cyberleaf.communitydrawing.drawingfragment.model.ModelListener;
import com.apps.cyberleaf.communitydrawing.drawingfragment.model.Point;

import java.util.ArrayList;


/**
 * Fragment qui represente la zone de paramettrage du dessin :
 * - la liste des couleurs
 * - le mot a dessiner
 * - le boutton clear pour effacer le dessin
 * - le boutton mot pour demander un nouveau mot
 */
public class ParameterZoneFragment extends Fragment implements ModelListener{

    private TextView word; // le textview qui permet d afficher le mot a dessiner
    private Button clearButton; // le bouton qui permet de nettoyuer la zone de dessin
    private Button sendButton;
    private Button reloadButton; //le bouton qui permet de charger un autre mot
    private ColorListView colorList; // la liste des couleurs

    private ArrayList<ParameterZoneListener> parameterListener = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.drawingfragment_parameter_view_layout, container, false);


        word = (TextView) view.findViewById(R.id.word);
        clearButton = (Button) view.findViewById(R.id.clearButton);
        reloadButton = (Button) view.findViewById(R.id.reloadButton);
        sendButton = (Button) view.findViewById(R.id.sendButton);
        colorList = (ColorListView) view.findViewById(R.id.listColor);
        initFragement();
        return view;
    }


    /**
     * Initialise le fragement :
     * met les listener sur les 2 boutons et la liste des couleurs
     *
     */
    private void initFragement() {
        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callbackClickOnClearButton();
            }
        });

        reloadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callbackClickOnNewWord();
            }
        });

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callbackClickOnSendButton();
            }
        });

        colorList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               callbackSelectColor(colorList.getintColor(position));
            }
        });
    }

    public void setWord(@NonNull String word){
        this.word.setText(word);
    }


    private void callbackClickOnSendButton(){
        //TODO : recupe le JSON et l'envoyé a la truc d'avant..
        DrawingMainActivity activity = (DrawingMainActivity) getActivity();
        activity.finishWithResult();
    }

    private void callbackClickOnClearButton(){
        for (ParameterZoneListener parameterZoneListener : parameterListener) {
            parameterZoneListener.onClickOnClearButton();
        }
    }

    private void callbackClickOnNewWord(){
        for (ParameterZoneListener parameterZoneListener : parameterListener) {
            parameterZoneListener.onClickOnNewWord();
        }
    }


    private void callbackSelectColor(int color){
        for (ParameterZoneListener parameterZoneListener : parameterListener) {
            parameterZoneListener.onSelectColor(color);
        }
    }

    public void addListener(ParameterZoneListener pzl){
        this.parameterListener.add(pzl);
    }


    @Override
    public void onAddToLine(Point lastpoint, Point newPoint, int color) {
        //to Do nothing : le seul callback qui nous interesse dans ce fragement est le onSetCurrentWord
    }

    @Override
    public void onNewLine(Point startPoint, int color) {
        //to Do nothing : le seul callback qui nous interesse dans ce fragement est le onSetCurrentWord
    }

    @Override
    public void onSetCurrentWord(String currentWord) {
        this.setWord(currentWord);
    }

    @Override
    public void onClear() {
        //to Do nothing : le seul callback qui nous interesse dans ce fragement est le onSetCurrentWord
    }
}

package com.apps.cyberleaf.communitydrawing.request;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

/**
 * Created by Eric on 26/03/2017.
 */

public class SubmitDrawRequest extends AbstractRequester {
    private final JSONObject drawing;

    public SubmitDrawRequest(Context context, String hostname, JSONObject drawing) {
        super(context, hostname);
        this.drawing = drawing;
    }

    @Override
    public String resolveURL(String... params) {
        if (params.length != 3) {
            throw new IllegalArgumentException("Params: token, latitude, longitude");
        }
        String token = params[0];
        String latitude = params[1];
        String longitude = params[2];
        String s = urlResolver.submitDraw(token, latitude, longitude);

        return s;
    }

    @Override
    public ResponseHolder request(String... params) throws VolleyError {
        String url = resolveURL(params);
        if(drawing == null) {
            Log.d("SubmitDrawRequest","drawing is null");
        }else {
            Log.d("SubmitDrawRequest","drawing is not null");
        }
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                url,
                drawing,
                getListener(),
                getErrorListener()
        );
        worker.addRequest(request);


        return getResponse();
    }
}

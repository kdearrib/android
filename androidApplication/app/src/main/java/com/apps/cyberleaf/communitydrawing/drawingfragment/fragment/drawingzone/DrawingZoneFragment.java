package com.apps.cyberleaf.communitydrawing.drawingfragment.fragment.drawingzone;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.apps.cyberleaf.communitydrawing.R;
import com.apps.cyberleaf.communitydrawing.drawingfragment.fragment.drawingzone.drawingview.DrawingView;
import com.apps.cyberleaf.communitydrawing.drawingfragment.model.ModelListener;
import com.apps.cyberleaf.communitydrawing.drawingfragment.model.ModelParameters;
import com.apps.cyberleaf.communitydrawing.drawingfragment.model.Point;

import java.util.ArrayList;


/**
 * Fragment qui permet concerne uniquement la zone de dessin
 */
public class DrawingZoneFragment extends Fragment {

    private DrawingView drawingZone; // la view qui permettra d afficher le dessin !
    private ArrayList<DrawingZoneListener> drawingZoneListeners = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // on recupere la vue qu on attendant pour le fargment
        View view = inflater.inflate(R.layout.drawingfragment_drawing_layout, container, false);

        drawingZone = (DrawingView) view.findViewById(R.id.drawingView);
        //on definit un listener sur les events lie a la vue du dessin
        drawingZone.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                onTouchDrawingZone(event);
                return true;
            }
        });

        return view;
    }


    /**
     * analyse de l event, en fonction de l'action on agit :
     *  - down : nouveau toucher sur la vue, ici un nouveau point
     *  - move : on se deplace sur la vue, ici un nouveau point sur la ligne
     *  - up : on releve le doigt
     * @param event
     */
    private void onTouchDrawingZone(MotionEvent event){
        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
                callbackNewTouchInZone(new Point(event.getX(),event.getY()));
                break;
            case MotionEvent.ACTION_MOVE:
                float x = event.getX();
                float y = event.getY();
                callbackMoveInZone(new Point(x,y));
                break;
            case MotionEvent.ACTION_UP:
                callbackUpDrawingZone();
                break;
            default:
        }

    }

    public void addListener(DrawingZoneListener dzl){
        this.drawingZoneListeners.add(dzl);
    }

    private void callbackUpDrawingZone(){
        for (DrawingZoneListener listener : drawingZoneListeners) {
            listener.onUpDrawingZone();
        }
    }

    private void callbackNewTouchInZone(Point p){
        for (DrawingZoneListener listener : drawingZoneListeners) {
            listener.onNewTouchInDrawingZone(p);
        }
    }

    private void callbackMoveInZone(Point p){
        for (DrawingZoneListener listener : drawingZoneListeners) {
            listener.onMoveOnDrawingZone(p);
        }
    }

    public ModelListener getViewAsModelListener(){
        return drawingZone;
    }

   public void setModelParameter(ModelParameters pararms){
       this.drawingZone.setModelParameters(pararms);
   }
}

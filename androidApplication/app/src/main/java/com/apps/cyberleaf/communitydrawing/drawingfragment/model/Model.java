package com.apps.cyberleaf.communitydrawing.drawingfragment.model;

import android.graphics.Color;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;



public class Model implements  ModelParameters {

    private ArrayList<Line> lines = new ArrayList<>(); // la liste des ligne
    private String currentWord = ""; // le mot courante

    private int width = -1; // la larguer de la fenetre de dessin, utile pour l'export JSON
    private int height = -1; // la hauteur de la fenetre de dessin, utile pour l'export JSON

    private int currentColor = Color.BLACK; // la couleur de la ligne courante
    private Point lastPoint = new Point(0,0); // le dernier point de la ligne courante
    private Line currendline = new Line(); // la ligne courante


    private List<ModelListener> modelListener = new ArrayList<>();


    public Model(){
    }

    public void setCurrentColor(int currentColor) {
        this.currentColor = currentColor;
    }

    /**
     * Ajout d'une nouvelle ligne :
     * @param root le point de base de la ligne courante !
     */
    public void addNewLine(Point root){
        //on creer une nouvelle lignes
        currendline = new Line();
        //on l'ajoute au ligne deja présente
        lines.add(currendline);
        //On set la couleur et on ajout le point
        currendline.setColor(currentColor);
        currendline.addPoint(root);
        lastPoint = root;
        for (ModelListener listener : modelListener) {
            listener.onNewLine(root,currentColor);
        }
    }

    /**
     * Ajout d'un nouveau point a la ligne courante !
     * @param point
     */
    public void addPoint(Point point){
        currendline.addPoint(point);
        for (ModelListener listener : modelListener) {
            listener.onAddToLine(lastPoint,point,currentColor);
        }
        lastPoint = point;
    }

    /**
     * Efface le model
     */
    public void clear(){
        this.lines.clear();
        for (ModelListener listener : modelListener) {
            listener.onClear();
        }
    }


    public void addModelListener(ModelListener modelListener) {
        this.modelListener.add(modelListener);
    }


    /**
     *  Change la taille pour de la fenetre de dessin
     *  /!\ recalcule tous les points pour se mettre a la taille (peut prendre du temps)
     * @param width la nouvelle largeur de la fenetre
     * @param height la nouvelle hauteur de la fenetre
     */
    @Override
    public void setSizeOfView(int width, int height) {
        if(!lines.isEmpty() && (width != this.width || height != this.height)){
            ArrayList<Line> copy = lines;
            lines = new ArrayList<>();



            for (Line line : copy) {
                int intColor = line.getIntColor();
                List<Point> points = line.getPoints();
                //TODO : faire l'homotécie sur les points !
                setCurrentColor(intColor);
                float firstX = points.get(0).getX() * width / this.width;
                float firstY = points.get(0).getY() * height / this.height;
                addNewLine(new Point(firstX,firstY));
                for (int i = 1; i < points.size(); i++) {
                    Point oldPoint = points.get(i);

                    float x = oldPoint.getX() * width / this.width;
                    float y = oldPoint.getY() * height / this.height;

                    Point newPoint = new Point(x , y);
                    addPoint(newPoint);
                }
            }
        }
        this.width = width;
        this.height = height;
    }

    public void setCurrentWord(String currentWord) {
        this.currentWord = currentWord;

    }

    public String getCurrentWord() {
        return currentWord;
    }

    /*
     * Code pour la persistance ! Traitement du JSON..
     */

    /**
     * Renvois le model sous format JSON
     * {
     *      "word": le_mot_courant(String),
     *      "height": la_hauteur(float ou double),
     *      "width": la_largeur(float ou double),
     *      "lines":[
     *      {
     *          "color":la_couleur_de_la_ligne(int)
     *          "points":[
     *          {
     *              "x": la_coord_horyz(int),
     *              "y": la_coord_verti(int)
     *          }
     *          ]
     *      },
     *      ]
     * }
     * @return le model au format JSON, chaine vide si erreur dans le model
     */
    public String getJsonStringForModel(){
        JSONObject global = new JSONObject();
        try {
            global.put("word", currentWord);
            global.put("height",height);
            global.put("width",width);
            JSONArray arrayLines = new JSONArray();
            for (Line line : lines) {
                JSONObject lineJson = new JSONObject();
                lineJson.put("color",line.getIntColor());
                JSONArray arrayPoint = new JSONArray();

                for (Point point : line.getPoints()) {
                    JSONObject pointJson = new JSONObject();
                    pointJson.put("x",point.getX());
                    pointJson.put("y",point.getY());
                    arrayPoint.put(pointJson);
                }
                lineJson.put("points",arrayPoint);
                arrayLines.put(lineJson);
            }
            global.put("lines",arrayLines);
            //return global.toString();
            return global.toString(4);
        } catch (JSONException e) {
            return  "";
        }
    }

    /**
     * creer / parametrise le model en fonction d'un model au format JSON
     * Si le JSON est pas au bon format : rien n'est fait au model
     * @param jsonValue la String au format JSON (cf @getJsonStringForModel pour le model)
     */
    public void initModelFromJson(String jsonValue){
        String wordFromJson = "";
        final ArrayList<Line> linesFromJson = new ArrayList<>();
        int widthFromJson = -1;
        int heightFromJson = -1;

        try {
            JSONObject global = new JSONObject(jsonValue);
            wordFromJson  = (String) global.get("word");
            widthFromJson = global.getInt("width");
            heightFromJson = global.getInt("height");
            JSONArray lineArray = global.getJSONArray("lines");
            for(int i = 0; i < lineArray.length(); i++){
                JSONObject lineJson = (JSONObject) lineArray.get(i);
                Line line = new Line();
                int color = lineJson.getInt("color");
                line.setColor(color);
                JSONArray pointArray = lineJson.getJSONArray("points");
                for(int j = 0; j < pointArray.length(); j++){
                    JSONObject pointJson = (JSONObject) pointArray.get(j);
                    float x = (float) pointJson.getDouble("x");
                    float y = (float) pointJson.getDouble("y");
                    line.addPoint(new Point(x,y));
                }
                linesFromJson.add(line);
            }

            this.lines =linesFromJson;
            this.setCurrentWord(wordFromJson);
            this.height = heightFromJson;
            this.width = widthFromJson;



        } catch (JSONException e) {
            e.printStackTrace();
        }






    }
}

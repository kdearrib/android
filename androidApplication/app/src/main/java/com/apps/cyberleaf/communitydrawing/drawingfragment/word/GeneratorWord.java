package com.apps.cyberleaf.communitydrawing.drawingfragment.word;

import android.content.Context;
import android.util.Log;

import com.apps.cyberleaf.communitydrawing.R;

import java.util.Random;

/**
 * Created by be5nx2 on 13/03/2017.
 */

public class GeneratorWord {
    private Context context;
    private  String dico[];

    public GeneratorWord(Context context){
        this.context = context;
        dico = context.getResources().getStringArray(R.array.dico);
        if (dico == null){

        }


    }

    public  String getNewRandomWord(){
        if (dico == null){
            Log.d("GeneratorWord","Dico is null");
        }
        int i = new Random().nextInt() % dico.length;
        if(i < 0) i = -i;
        return dico[i];

    }
}

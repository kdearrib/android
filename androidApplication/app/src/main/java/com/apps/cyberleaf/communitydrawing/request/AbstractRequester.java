package com.apps.cyberleaf.communitydrawing.request;

import android.content.Context;
import android.text.LoginFilter;
import android.util.Log;

import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;


/**
 * Created by Eric on 26/03/2017.
 */

public abstract class AbstractRequester implements Requester {
    final URLResolver urlResolver;
    final RequestWorker worker;
    private final ResponseHolder responseHolder = new ResponseHolder();

    AbstractRequester(Context context, String hostname) {
        this.urlResolver = new URLResolver(hostname);
        this.worker = RequestWorker.getInstance(context);
    }

    @Override
    public ResponseHolder request(String... params) throws VolleyError {
        String url = resolveURL(params);
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        responseHolder.value = response;
                    }
                },
                getErrorListener()
        );
        worker.addRequest(request);

        return getResponse();
    }


    ResponseHolder getResponse() throws VolleyError {
        if (responseHolder.error != null) {
            throw responseHolder.error;
        }
        return responseHolder;
    }

    Response.Listener<JSONObject> getListener() {

        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                responseHolder.value = response;
            }
        };
    }

    Response.ErrorListener getErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                responseHolder.error = error;
            }
        };
    }
}

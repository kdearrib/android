package com.apps.cyberleaf.communitydrawing.request;

import android.content.Context;

/**
 * Created by Eric on 26/03/2017.
 */

public class AllDrawsRequest extends AbstractRequester {

    public AllDrawsRequest(Context context, String hostname) {
        super(context, hostname);
    }

    @Override
    public String resolveURL(String... params) {
        if (params.length != 3) {
            throw new IllegalArgumentException("Params: token, latitude, longitude");
        }
        String token = params[0];
        String latitude = params[1];
        String longitude = params[2];
        return urlResolver.getAllDraws(token, latitude, longitude);
    }
}


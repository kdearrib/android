package com.apps.cyberleaf.communitydrawing.connectionfragment;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.LoginFilter;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.apps.cyberleaf.communitydrawing.R;
import com.apps.cyberleaf.communitydrawing.request.RequestWorker;
import com.apps.cyberleaf.communitydrawing.request.ResponseHolder;
import com.apps.cyberleaf.communitydrawing.request.SignInRequest;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by be5nx2 on 27/03/2017.
 */

public class ConnectionMainActivity extends Activity {

    private Button signinButton;
    private EditText login;
    private EditText password;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.connectionfragment_activity_main);

        signinButton = (Button) findViewById(R.id.sign_in_button);
        login = (EditText) findViewById(R.id.connection_login_edittext);
        password = (EditText) findViewById(R.id.connection_password_edittext);

        signinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });


    }


    private void signIn(){
        final String logginString = login.getText().toString();
        final String passWordString = password.getText().toString();
        Log.d("ConnectionMainActivity", "Sign in : login : " + logginString + ", pwd : " + passWordString);

        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.GET,
                new SignInRequest(this, "http://kdearrib.ovh:7777").resolveURL("", logginString, passWordString),
                null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("SIGNIN", "onResponse: " + response);
                        try {
                            if(response.getInt("error") == 0) {
                                addLoginToSharedPreference(logginString);
                                finish();
                            } else {
                                login.setText("");
                                password.setText("");
                                Toast.makeText(ConnectionMainActivity.this, "Connexion impossible", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            login.setText("");
                            password.setText("");
                            Toast.makeText(ConnectionMainActivity.this, "Connexion impossible", Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        login.setText("");
                        password.setText("");
                    }
                });

        RequestWorker.getInstance(this).addRequest(request);



//        SignInRequest signInRequest = new SignInRequest(this, "http://kdearrib.ovh:7777");
//        try {
//            ResponseHolder responseHolder = signInRequest.request("", logginString, passWordString);
//
//
//            JSONObject value = responseHolder.getValue();
//
//            while (value == null){
//                value = responseHolder.getValue();
//                try {
//                    Thread.sleep(100);
//                } catch (InterruptedException e) {
//
//                }
//            }
//            int error = value.getInt("error");
//            if(error == 0){
//                Toast.makeText(this,getString(R.string.connected),Toast.LENGTH_SHORT).show();
//                //TODO: inserer dans les SharedPreff le loggin
//                addLoginToSharedPreference(logginString);
//            }else {
//                Toast.makeText(this,getString(R.string.notconnected),Toast.LENGTH_SHORT).show();
//            }
//
//
//        } catch (VolleyError volleyError) {
//            //volleyError.printStackTrace();
//            Toast.makeText(this, getString(R.string.notconnected), Toast.LENGTH_SHORT).show();
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
    }

    private void addLoginToSharedPreference(@NonNull String login){
        SharedPreferences prefs = getSharedPreferences("user_prefs", MODE_PRIVATE);
        prefs.edit().putString("login",login).commit();
    }

}

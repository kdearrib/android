package com.apps.cyberleaf.communitydrawing.gridviewfragment.fragment.gridview;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.apps.cyberleaf.communitydrawing.R;
import com.apps.cyberleaf.communitydrawing.gridviewfragment.GridviewMainActivity;
import com.apps.cyberleaf.communitydrawing.request.AllDrawsRequest;
import com.apps.cyberleaf.communitydrawing.request.RequestWorker;
import com.apps.cyberleaf.communitydrawing.request.ResponseHolder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by tkieffer on 24/03/17.
 */
public class GridViewFragment extends Fragment{

    private GridView gridView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.gridviewfragment_gridview_fragment, container, false);
        gridView = (GridView) view.findViewById(R.id.gridView_in_fragment);

        //view.setBackgroundColor(Color.RED);

        //String toto[] = {"toto","toto","toto","toto","toto","toto","toto","toto","toto","toto","toto","toto","toto","toto","toto","toto","toto","toto","toto","toto","toto","toto","toto","toto","toto","toto","toto","toto","toto","toto","toto","toto","toto"};
//        String a =getString(R.string.dessin1);
//       final String tab[]=  new String[10];
//        //final String tab[]= getAllDrawsFromServeur();
//       for(int i =0; i < tab.length; tab[i]=a , i++ );

        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.GET,
                new AllDrawsRequest(this.getActivity(), "http://kdearrib.ovh:7777").resolveURL("token", "34", "34"),
                null,
                new Response.Listener<JSONObject> () {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray draws = response.getJSONArray("draws");

                            final String array[] = new String[draws.length()];

                            for(int i = 0; i < draws.length(); i++) {

                                //On récupère un JsonArray depuis le JsonArray
                                //On doit caster, fragile en cas de changement de mise en forme
                                //Car c'est un cast sale
                                //indice 0 = id
                                //indice 1 = json du dessin
                                array[i] = ((JSONArray) draws.get(i)).getString(1);

                            }

                            final DrawingAdapter adapter = new DrawingAdapter(getActivity(), android.R.layout.activity_list_item, array);



                            gridView.setAdapter(adapter);

                            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    ((GridviewMainActivity)getActivity()).changeToAnswerView(array[position]);
                                }
                            });

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                });

        RequestWorker.getInstance(this.getActivity()).addRequest(request);

        return view;

    }

}

package com.apps.cyberleaf.communitydrawing.service;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

import com.android.volley.VolleyError;
import com.apps.cyberleaf.communitydrawing.R;
import com.apps.cyberleaf.communitydrawing.gridviewfragment.GridviewMainActivity;
import com.apps.cyberleaf.communitydrawing.request.AllDrawsRequest;
import com.apps.cyberleaf.communitydrawing.request.Requester;
import com.apps.cyberleaf.communitydrawing.request.ResponseHolder;
import org.json.JSONException;
import org.json.JSONObject;
public class NetworkService extends Service {
    private static final int NOTIFICATION_ID = 0;
    public NetworkService() {
    }
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        int i = super.onStartCommand(intent, flags, startId);
        final Requester request = new AllDrawsRequest(this, "kdearrib.ovh:7777");
        final String token = intent.getStringExtra("token");
        final String lat = intent.getStringExtra("lat");
        final String lon = intent.getStringExtra("lon");

        Runnable r = new Runnable() {
            @Override
            public void run() {

                try {
                    ResponseHolder responseHolder = request.request(token, lat, lon);
                    JSONObject value = responseHolder.getValue();
                    int nb = value.getJSONArray("array").length();
                    if(nb > 0) {
                        buildNotification(nb);
                    }
                } catch (JSONException e) {
                } catch (VolleyError volleyError) {
                    //.printStackTrace();
                }
                stopSelf();
            }
        };

        new Thread(r).start();
        return i;
    }
    private void buildNotification(int nb) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_brush_white_24dp)
                        .setContentTitle("Nouveautées")
                        .setContentText(nb + " nouveaux dessins sont disponibles !");
        Intent resultIntent = new Intent(this, GridviewMainActivity.class);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0, resultIntent, 0);
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }
}
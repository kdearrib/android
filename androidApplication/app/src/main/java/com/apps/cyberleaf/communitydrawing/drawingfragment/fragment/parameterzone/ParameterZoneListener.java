package com.apps.cyberleaf.communitydrawing.drawingfragment.fragment.parameterzone;


/**
 * Permet d implanter un listener sur le fragment de la zone de parametre
 */
public interface ParameterZoneListener {

    /**
     * appeler si on applique sur le bouton clear
     */
    void onClickOnClearButton();

    /**
     * appeler si on applique sur le bouton pour un nouveau mot
     */
    void onClickOnNewWord();

    /**
     *  appeler quand on change la couleur
     * @param colorInt la couleur selectionnee
     */
    void onSelectColor(int colorInt);
}

package com.apps.cyberleaf.communitydrawing.request;

/**
 * Resolves URL of requests.
 * Created by Eric on 26/03/2017.
 */
public class URLResolver {
    private final String hostname;

    URLResolver(String hostname) {
        this.hostname = hostname;
    }

    /**
     * Get the URL for signing up request.
     * @param username Username.
     * @param login Login of the user.
     * @param hashPassword SHA-1 password of the user.
     * @return The URL of request.
     */
    public String signUp(String username, String login, String hashPassword) {
        return hostname + "/api/signup?username=" + username + "&login=" + login + "&hashpassword=" + hashPassword;
    }

    /**
     * Get the URL for signing in request.
     * @param username The username.
     * @param login User login.
     * @param hashPassword User SHA-1 password.
     * @return The URL of request.
     */
    public String signIn(String username, String login, String hashPassword) {

        return hostname + "/api/signin?username=" + username + "&login=" + login + "&hashpassword=" + hashPassword;
    }

    /**
     * Get the URL for getting all draws.
     * @param token User token.
     * @param latitude Current latitude of user.
     * @param longitude Current longitude of user.
     * @return The URL of request.
     */
    public String getAllDraws(String token, String latitude, String longitude) {
        return hostname + "/api/getAllDraws?token=" + token + "?lat=" + latitude + "?lon=" + longitude;
    }

    /**
     * Get the URL for submitting an answer.
     * @param token User token.
     * @param drawId Drawing ID.
     * @return The URL of request.
     */
    public String submitResponse(String token, String drawId) {
        return hostname + "/api/submitResponse?token=" + token + "&drawId=" + drawId;
    }

    /**
     * Get the URL for submitting a drawing.
     * @param token User token.
     * @param latitude Current latitude of user.
     * @param longitude Current longitude of user.
     * @return The URL of request.
     */
    public String submitDraw(String token, String latitude, String longitude) {
        return hostname + "/api/submitDraw?token=" + token + "&lat=" + latitude + "&lon=" + longitude;
    }
}

package com.apps.cyberleaf.communitydrawing.drawingfragment.fragment.parameterzone.listcolorview;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.apps.cyberleaf.communitydrawing.R;

/**
 * Represente la liste des couleurs !
 */
public class ColorListView extends ListView {

    private static final ColorView[] colorViews = ColorView.values(); // La liste des couleurs disponnibles
    private String[] colorsName; // la liste des noms des couleurs disponnible
    private ArrayAdapter<String> arrayAdapter; // adaptateur pour afficher le nom de la liste



    public ColorListView(Context context) {
        super(context);
        initListView();
    }

    public ColorListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initListView();
    }

    public ColorListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initListView();
    }

    /**
     * renvois la couleur attentu au champs position, sous forme de int
     * @param position la position de la couleur dans la liste
     * @return la valeur numerique d un entier
     */
    public int getintColor(int position){
        if(position < 0 || position > colorViews.length){
            throw new ArrayIndexOutOfBoundsException();
        }
        return ContextCompat.getColor(getContext(), colorViews[position].idColor);
    }

    /**
     * initialise le tableau de nom de couleur
     */
    private void initStringColorName(){
        colorsName =  new String[colorViews.length];
        for(int i = 0; i < colorViews.length; ++i){
            colorsName[i] = getStingColor(i);
        }
    }

    /**
     * Renvois le nom de la couleur pour une position donnee
     * @param position l indice de la couleur
     * @return un String qui correspond au nom de la couleur
     */
    public String getStingColor(int position){
        if(position < 0 || position > colorViews.length){
            throw new ArrayIndexOutOfBoundsException();
        }
        return getResources().getString(colorViews[position].idRessource);
    }

    /**
     * initialise la list des noms !
     */
    private void initListView(){

        initStringColorName();
        arrayAdapter = new ArrayAdapter<String>(getContext(),android.R.layout.simple_list_item_1,colorsName);
        setAdapter(arrayAdapter);

    }




    private enum ColorView{

        BLACK(R.string.COLOR_BLACK, R.color.COLOR_BLACK),
        WHITE(R.string.COLOR_WHITE, R.color.COLOR_WHITE),
        BLUE(R.string.COLOR_BLUE, R.color.COLOR_BLUE),
        GREEN(R.string.COLOR_GREEN, R.color.COLOR_GREEN),
        GRAY(R.string.COLOR_GRAY, R.color.COLOR_GRAY),
        RED(R.string.COLOR_RED, R.color.COLOR_RED),
        YELLOW(R.string.COLOR_YELLOW, R.color.COLOR_YELLOW),
        ORANGE(R.string.COLOR_ORANGE, R.color.COLOR_ORANGE),
        PURPLE(R.string.COLOR_PURPLE, R.color.COLOR_PURPLE),
        BROWN(R.string.COLOR_BROWN, R.color.COLOR_BROWN),
        ROSE(R.string.COLOR_PINK, R.color.COLOR_PINK);


        private final int idRessource;
        private final int idColor;

        ColorView(int idRessource, int idColor){
            this.idRessource = idRessource;
            this.idColor = idColor;
        }

        public int getIdColor(){
            return idColor;
        }

        public int getName() {
            return idRessource;
        }


    }

}

package com.apps.cyberleaf.communitydrawing.request;

import com.android.volley.VolleyError;

import org.json.JSONObject;

/**
 * Created by Eric on 26/03/2017.
 */

public class ResponseHolder {
    JSONObject value;
    VolleyError error;

    public JSONObject getValue() {
        return value;
    }

    public VolleyError getError() {
        return error;
    }
}

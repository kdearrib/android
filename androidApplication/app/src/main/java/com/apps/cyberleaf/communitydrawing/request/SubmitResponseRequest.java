package com.apps.cyberleaf.communitydrawing.request;

import android.content.Context;

/**
 * Created by Eric on 26/03/2017.
 */

public class SubmitResponseRequest extends AbstractRequester {

    public SubmitResponseRequest(Context context, String hostname) {
        super(context, hostname);
    }

    @Override
    public String resolveURL(String... params) {
        if (params.length != 2) {
            throw new IllegalArgumentException("Params: token, drawId");
        }
        String token = params[0];
        String drawId = params[1];
        return urlResolver.submitResponse(token, drawId);
    }
}

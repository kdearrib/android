package com.apps.cyberleaf.communitydrawing.request;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Handle requests of the application.
 * Singleton class.
 * Created by Eric on 26/03/2017.
 */
public class RequestWorker {
    private static final Object lock = new Object();
    private static RequestWorker instance;
    private final RequestQueue requestQueue;

    private RequestWorker(Context context) {
        this.requestQueue = Volley.newRequestQueue(context.getApplicationContext());
    }

    /**
     * Get an instance of the RequestWorker.
     * @param context Application context.
     * @return The instance of the RequestWorker.
     */
    public static RequestWorker getInstance(Context context) {
        synchronized (lock) {
            if (instance == null) {
                instance = new RequestWorker(context);
            }
            return instance;
        }
    }

    /**
     * Adds a request to handle.
     * @param request Request to add.
     * @param <T> Response type of request.
     */
    public <T> void addRequest(Request<T> request) {
        requestQueue.add(request);
    }
}

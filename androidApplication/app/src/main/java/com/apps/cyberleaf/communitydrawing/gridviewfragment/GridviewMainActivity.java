package com.apps.cyberleaf.communitydrawing.gridviewfragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

import com.apps.cyberleaf.communitydrawing.R;
import com.apps.cyberleaf.communitydrawing.gridviewfragment.fragment.answer.AnswerInteraction;
import com.apps.cyberleaf.communitydrawing.gridviewfragment.fragment.answer.AnswerView;
import com.apps.cyberleaf.communitydrawing.gridviewfragment.fragment.gridview.GridViewFragment;

/**
 * Created by tkieffer on 24/03/17.
 */
public class GridviewMainActivity extends FragmentActivity {


    private AnswerView answerView; // fragment pour la vue du dessin
    private AnswerInteraction answerInteraction; // fragment pour l'interaction avec le dessin
    private GridViewFragment gridViewFragment; // fragment pour la grille de vue

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gridviewfragment_grid_activity_main);

        gridViewFragment = (GridViewFragment) getFragmentManager().findFragmentById(R.id.gridview_fragment_in_activity);

    }

    public void changeToAnswerView(String modelJSON){

        setContentView(R.layout.gridviewfragment_answer_activity_main);
        answerView = (AnswerView) getFragmentManager().findFragmentById(R.id.viewfragment_in_answer);
        answerInteraction = (AnswerInteraction) getFragmentManager().findFragmentById(R.id.interaction_answer);

        answerView.setTextModel(modelJSON);
        answerInteraction.setCurrentWord(answerView.getCurrentWord());

    }


}

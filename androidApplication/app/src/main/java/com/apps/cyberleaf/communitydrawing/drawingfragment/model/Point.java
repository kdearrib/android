package com.apps.cyberleaf.communitydrawing.drawingfragment.model;



import java.util.ArrayList;
import java.util.List;

/**
 * Represente un point dans le model et dans la vue
 */
public class Point {
    private float x;
    private float y;

    public Point(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    /**
     * Renvois le point median entre le point courant et p2
     * @param p2 le second point auquel on veut le point du milieu
     * @return
     */
    public Point getMiddlePointBetween(Point p2){
        return new Point((getX() + p2.getX()) / 2, (getY() + p2.getY()) / 2);
    }

    /**
     *
     * @param p2
     * @param expectedDistance
     * @return true si la distance entre p2 et this est  < a expectedDistance, false sinon
     */
    public boolean isEnoughClose(Point p2, int expectedDistance){

       return  getSquareDistance(p2) <= (expectedDistance * expectedDistance);
    }

    public float getSquareDistance(Point p2){
        float diffX = getX() - p2.getX();
        float diffY = getY() - p2.getY();
        return (diffX * diffX) + (diffY * diffY);
    }



}

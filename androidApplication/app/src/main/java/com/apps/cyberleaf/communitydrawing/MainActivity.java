package com.apps.cyberleaf.communitydrawing;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


import com.android.volley.VolleyError;
import com.apps.cyberleaf.communitydrawing.connectionfragment.ConnectionMainActivity;
import com.apps.cyberleaf.communitydrawing.drawingfragment.DrawingMainActivity;
import com.apps.cyberleaf.communitydrawing.gridviewfragment.GridviewMainActivity;
import com.apps.cyberleaf.communitydrawing.location.LocationRetriever;
import com.apps.cyberleaf.communitydrawing.request.Requester;
import com.apps.cyberleaf.communitydrawing.request.SubmitDrawRequest;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    private Button drawingButton;
    private Button guessButton;
    private LocationRetriever locationRetriever;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //TODO : demander la connection + Token !

        SharedPreferences prefs = getSharedPreferences("user_prefs", MODE_PRIVATE);
        String login = prefs.getString("login", null);
        if(login == null){
            Toast.makeText(this,getString(R.string.notconnected), Toast.LENGTH_SHORT).show();
            startActivity(new Intent(this, ConnectionMainActivity.class));
            //// TODO: 27/03/2017 demander la connection ici (en lancant l'activité)
        }else{
            Toast.makeText(this,getString(R.string.welcomme) + " " + login,Toast.LENGTH_SHORT).show();
        }
        
       /* if(true){
            startActivity(new Intent(this, ConnectionMainActivity.class));
        }*/

        if(locationRetriever == null){
            locationRetriever = new LocationRetriever(this);
        }
        checkPermissions();

        setContentView(R.layout.main_layout);
        guessButton = (Button) findViewById(R.id.guessButton);
        drawingButton = (Button) findViewById(R.id.drawingButton);

        initClickListener();



    }


    private void initClickListener(){
        final Intent drawingIntent = new Intent(this,DrawingMainActivity.class);
        drawingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivityForResult(drawingIntent, 42);

            }
        });



        final Intent guessIntent = new Intent(this, GridviewMainActivity.class);
        guessButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),R.string.loadDraws,Toast.LENGTH_SHORT).show();

                startActivity(guessIntent);
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {



        if(requestCode == 42){
            if(resultCode == Activity.RESULT_OK){
                String drawing = data.getStringExtra("DRAWING");
                try {
                    Requester requester = new SubmitDrawRequest(this,"http://kdearrib.ovh:7777",new JSONObject(drawing));
                    requester.request("55","3434","3434" );
                    Toast.makeText(this, getString(R.string.drawingsSend), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    Toast.makeText(this,getString(R.string.impossibleDrawingSend), Toast.LENGTH_SHORT).show();
                } catch (VolleyError volleyError) {
                    volleyError.printStackTrace(); // pour le request()
                }

            }
        }


        super.onActivityResult(requestCode, resultCode, data);
    }



    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case LocationRetriever.PERMISSION_LOCATION_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    
                    locationRetriever.startGPSListener();

                } else {
                    Toast.makeText(this, getString(R.string.GPSactivate), Toast.LENGTH_SHORT).show();

                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void checkPermissions() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, LocationRetriever.PERMISSION_LOCATION_REQUEST_CODE);

            return;
        }
    }

}
package com.apps.cyberleaf.communitydrawing.drawingfragment.model;


/**
 * Classe qui permet de suivre l etat du model et son evolution
 */
public interface ModelListener {

    /**
     * Appeler quand on ajoute un point newPoint a une ligne
     * @param lastpoint le dernier point ajoute avant le point newPoint
     * @param newPoint le nouveau point ajouter
     * @param color la couleur de la ligne au quel le point est accroche
     */
    void onAddToLine(Point lastpoint, Point newPoint, int color);

    /**
     * Appeler quand on creer une nouvelle ligne
     * @param startPoint le point de depart de la nouvelle ligne
     * @param color la couleur de la ligne
     */
    void onNewLine(Point startPoint, int color);

    /**
     * Appeler quand on changer le mot dans le model
     * @param currentWord le nouveau mot qu'on met
     */
    void onSetCurrentWord(String currentWord);

    /**
     * Appeler quand on vide le model
     */
    void onClear();
}

package com.apps.cyberleaf.communitydrawing.drawingfragment.model;

import android.graphics.Color;

import java.util.ArrayList;
import java.util.List;


/**
 * Classe qui represente une ligne a l affichage : un liste de point et une couleur
 */
public class Line{

    private ArrayList<Point> points = new ArrayList<>();
    private int color = Color.BLACK;

    public List<Point> getPoints() {
        return points;
    }

    public int getIntColor(){
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public void addPoint(Point p){
        points.add(p);
    }

    public static Line copyOf(Line line){
        Line copy = new Line();
        copy.points.addAll(line.points);
        copy.color = line.getIntColor();
        return copy;
    }
}

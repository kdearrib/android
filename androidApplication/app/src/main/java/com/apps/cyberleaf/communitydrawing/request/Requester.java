package com.apps.cyberleaf.communitydrawing.request;

import com.android.volley.VolleyError;

/**
 * Interface for requesters.
 * Created by Eric on 26/03/2017.
 */

public interface Requester {

    /**
     * Resolve request string.
     * @param params Parameters of the request.
     * @return The request string.
     */
    String resolveURL(String... params);

    /**
     * Request the request to the server.
     * @param params Parameters of the request.
     * @return The response into a holder.
     * @throws VolleyError if a network error occurred.
     */
    public ResponseHolder request(String... params) throws VolleyError;
}

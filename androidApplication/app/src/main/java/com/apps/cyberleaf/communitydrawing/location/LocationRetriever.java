package com.apps.cyberleaf.communitydrawing.location;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.apps.cyberleaf.communitydrawing.R;

/**
 * Created by kevin on 24/03/17.
 */

public class LocationRetriever {

    public static final int PERMISSION_LOCATION_REQUEST_CODE = 0;
    private LocationManager locationManager;
    private LocationListener locationListener;
    private final String TAG = "LOCATION";
    private Location bestCurrentLocation;
    private final Context context;

    public LocationRetriever(Context context) {

        this.context = context;
    }

    public void startGPSListener() {

        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        // Define a listener that responds to location updates
        locationListener = new LocationListener() {

            public void onLocationChanged(Location location) {
                //Toast.makeText(MainActivity.this, "New location (LAT: " + location.getLatitude() + ", LONG: " + location.getLongitude() , Toast.LENGTH_SHORT).show();
                Log.d(TAG, "onLocationChanged: New location (LAT: " + location.getLatitude() + ", LONG: " + location.getLongitude());

                //On va comparer avec la bestCurrentLocation
                if(isBetterLocation(location, bestCurrentLocation)) {
                    bestCurrentLocation = location;
                }

            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
                Log.d(TAG, "onStatusChanged: " + provider + " Status: " + status);
            }

            public void onProviderEnabled(String provider) {
                Log.d(TAG, "onProviderEnabled: " + provider);
            }

            public void onProviderDisabled(String provider) {
                Log.d(TAG, "onProviderDisabled: " + provider);
            }
        };

        // Register the listener with the Location Manager to receive location updates
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);

        String bestProvider = locationManager.getBestProvider(criteria, false);

        try {
            bestCurrentLocation = locationManager.getLastKnownLocation(bestProvider);

            locationManager.requestLocationUpdates(bestProvider, 0, 0, locationListener);
        } catch (SecurityException e) {
            Log.d(TAG, "startGPSListener: Permissions have not been granted");
            Toast.makeText(context, context.getString(R.string.NotLocalisation), Toast.LENGTH_SHORT).show();
        }

    }


    public void stopGPSListener() {

        if(locationManager == null || locationListener == null) {
            return;
        }

        // Remove the listener you previously added
        locationManager.removeUpdates(locationListener);


    }

    private static final int DELTA_TIME = 1000 * 60 * 2;

    /** Determines whether one Location reading is better than the current Location fix
     * @param location  The new Location that you want to evaluate
     * @param currentBestLocation  The current Location fix, to which you want to compare the new one
     */
    private boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > DELTA_TIME;
        boolean isSignificantlyOlder = timeDelta < -DELTA_TIME;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /** Checks whether two providers are the same */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    /**
     * Get the location that is currently the best
     * @return
     */
    public Location getBestCurrentLocation() {
        return bestCurrentLocation;
    }


}

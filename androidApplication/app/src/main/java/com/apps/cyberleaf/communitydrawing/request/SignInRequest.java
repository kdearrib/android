package com.apps.cyberleaf.communitydrawing.request;

import android.content.Context;
import android.util.Log;

import static android.content.ContentValues.TAG;

/**
 * Sign in request.
 * Created by Eric on 26/03/2017.
 */

public class SignInRequest extends AbstractRequester {
    public SignInRequest(Context context, String hostname) {
        super(context, hostname);
    }

    @Override
    public String resolveURL(String... params)  {
        if (params.length != 3) {
            throw new IllegalArgumentException("Params: username, login, hashPassword");
        }
        String username = params[0];
        String login = params[1];
        String hashPassword = params[2];

        return urlResolver.signIn(username, login, hashPassword);
    }
}

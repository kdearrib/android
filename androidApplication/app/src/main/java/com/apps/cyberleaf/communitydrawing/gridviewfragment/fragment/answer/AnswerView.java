package com.apps.cyberleaf.communitydrawing.gridviewfragment.fragment.answer;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apps.cyberleaf.communitydrawing.R;
import com.apps.cyberleaf.communitydrawing.drawingfragment.fragment.drawingzone.drawingview.DrawingView;
import com.apps.cyberleaf.communitydrawing.drawingfragment.model.Model;

/**
 * Created by be5nx2 on 26/03/2017.
 */

public class AnswerView extends Fragment {

    private DrawingView drawingView;
    private Model model = new Model();



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.gridviewfragment_view_fragment,container,false);

        drawingView = (DrawingView) view.findViewById(R.id.drawingView_in_view_grid_fragment);
        drawingView.setBackgroundColor(Color.WHITE);

        drawingView.setModelParameters(model);
        model.addModelListener(drawingView);


        return view;

    }

    public void setTextModel(String JSON){
        model.initModelFromJson(JSON);
    }

    public String getCurrentWord(){
        return model.getCurrentWord();
    }
}

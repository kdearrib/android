package com.apps.cyberleaf.communitydrawing.gridviewfragment.fragment.answer;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.apps.cyberleaf.communitydrawing.R;
import com.apps.cyberleaf.communitydrawing.request.SubmitResponseRequest;

/**
 * Created by be5nx2 on 26/03/2017.
 */

public class AnswerInteraction extends Fragment {

    private String currentWord = "";
    private EditText editText;
    private Button submitButton;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.gridviewfragment_answer_fragment, container, false);

        editText = (EditText) view.findViewById(R.id.answerText);

        submitButton = (Button) view.findViewById(R.id.submitButton);

        final Activity activity = this.getActivity();

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = editText.getText().toString();
                if(isValideWord(text)){
                    Toast.makeText(getActivity(), getString(R.string.goodWord), Toast.LENGTH_SHORT).show();

                    //On récupère le login

                    //on l'envoi au server
                    try {
                        new SubmitResponseRequest(activity, "http://kdearrib.ovh:7777").request("token", "0");
                    } catch (VolleyError volleyError) {
                        volleyError.printStackTrace();
                    }

                    activity.finish();

                }else {
                    Toast.makeText(getActivity(),getString(R.string.wrongWord), Toast.LENGTH_SHORT).show();
                    // TODO: 28/03/2017 trouver le bon mot ..
                    editText.setText("");

                }
            }
        });


        return view;
    }

    public void setCurrentWord(String currentWord) {
        this.currentWord = currentWord;
    }

    private boolean isValideWord(String input){
        return input.toLowerCase().equals(currentWord.toLowerCase());
    }


}

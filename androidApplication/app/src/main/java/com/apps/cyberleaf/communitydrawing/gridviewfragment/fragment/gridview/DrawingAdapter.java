package com.apps.cyberleaf.communitydrawing.gridviewfragment.fragment.gridview;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.apps.cyberleaf.communitydrawing.drawingfragment.fragment.drawingzone.drawingview.DrawingView;
import com.apps.cyberleaf.communitydrawing.drawingfragment.model.Model;

/**
 * Created by tkieffer on 24/03/17.
 */
public class DrawingAdapter extends ArrayAdapter {




    private final Context context;

    private final String model[];
    private final DrawingView views[];

    public DrawingAdapter(Context context, int resource, Object[] objects) {
        super(context, resource, objects);
        this.context = context;
        this.model = (String[]) objects;
        views = new DrawingView[model.length];
        initViews();
    }


    private void initViews(){
        for(int i = 0; i < model.length;i++){
            DrawingView drawingView = new DrawingView(context){
                /**
                 * Je surcharge a la volée uniquement pour éviter d'implémenté une autre view..
                 * En effet pour utiliser la même view que celle dans DrawingZone, on s'est mis d'accord
                 * sur plusieurs méthode a mettre en protected et redéfinir afin de ce servir du model
                 * et de la View du fragment DrawingFragment
                 */

                @Override
                protected ViewGroup.LayoutParams getCurrentLayoutParams(int width, int height) {
                    return new AbsListView.LayoutParams(width,height);
                }

                @Override
                protected int getStrokeWidth() {
                    return 3;
                }
            };


            drawingView.setBackgroundColor(Color.WHITE);
            Model model = new Model();
            model.addModelListener(drawingView);
            drawingView.setModelParameters(model);
            drawingView.setLayoutParams(new ViewGroup.LayoutParams(300,300));
            model.initModelFromJson(this.model[i]);

            views[i] = drawingView;
        }
    }




    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
       // return super.getView(position, convertView, parent);


        if (views[position]!= null){
            return views[position];
        }else{
            TextView textView = new TextView(getContext());
            textView.setText("Empty");
            textView.setBackgroundColor(Color.WHITE);
            return textView;
        }

    }

}

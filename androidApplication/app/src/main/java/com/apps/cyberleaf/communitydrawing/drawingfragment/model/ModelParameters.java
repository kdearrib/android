package com.apps.cyberleaf.communitydrawing.drawingfragment.model;


/**
 * Classe qui permet de paramettre le model, en l'occurence ne permet de que de modifier la taille de la fenetre
 */
public interface ModelParameters {

    void setSizeOfView(int width, int height);
}

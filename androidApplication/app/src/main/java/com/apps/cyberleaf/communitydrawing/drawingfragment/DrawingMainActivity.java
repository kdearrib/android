package com.apps.cyberleaf.communitydrawing.drawingfragment;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.apps.cyberleaf.communitydrawing.R;
import com.apps.cyberleaf.communitydrawing.drawingfragment.fragment.drawingzone.DrawingZoneFragment;
import com.apps.cyberleaf.communitydrawing.drawingfragment.fragment.drawingzone.DrawingZoneListener;
import com.apps.cyberleaf.communitydrawing.drawingfragment.fragment.parameterzone.ParameterZoneFragment;
import com.apps.cyberleaf.communitydrawing.drawingfragment.fragment.parameterzone.ParameterZoneListener;
import com.apps.cyberleaf.communitydrawing.drawingfragment.model.Model;
import com.apps.cyberleaf.communitydrawing.drawingfragment.model.Point;
import com.apps.cyberleaf.communitydrawing.drawingfragment.word.GeneratorWord;


public class DrawingMainActivity extends FragmentActivity implements DrawingZoneListener,ParameterZoneListener{



    private  DrawingZoneFragment drawingFragment;
    private ParameterZoneFragment parameteFragment;
    private Model model;
    private GeneratorWord generatorWord;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.drawingfragment_activity_main);
        drawingFragment = (DrawingZoneFragment) getFragmentManager().findFragmentById(R.id.DrawindZoneFrament);
        model = new Model();
        parameteFragment = (ParameterZoneFragment) getFragmentManager().findFragmentById(R.id.ParameterZoneFragment);

        drawingFragment.addListener(this);
        drawingFragment.setModelParameter(model);
        parameteFragment.addListener(this);
        model.addModelListener(drawingFragment.getViewAsModelListener());
        model.addModelListener(parameteFragment);


        if(savedInstanceState == null){
            onClickOnNewWord();
        }




    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        String jsonStringForModel = model.getJsonStringForModel();
        outState.putString("MODEL",jsonStringForModel);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        String modelSave = savedInstanceState.getString("MODEL");
        model.initModelFromJson(modelSave);
        parameteFragment.setWord(model.getCurrentWord());
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onNewTouchInDrawingZone(Point p) {
        model.addNewLine(p);
    }

    @Override
    public void onMoveOnDrawingZone(Point p) {
        model.addPoint(p);
    }

    @Override
    public void onUpDrawingZone() {
    }

    @Override
    public void onClickOnClearButton() {
        model.clear();

    }

    @Override
    public void onClickOnNewWord() {
        generatorWord = new GeneratorWord(this);
        String word = generatorWord.getNewRandomWord();
        model.setCurrentWord(word);
        parameteFragment.setWord(word);
    }

    @Override
    public void onSelectColor(int colorInt) {
        model.setCurrentColor(colorInt);
    }

    public void finishWithResult(){
        Intent returnedIntent = new Intent();
        returnedIntent.putExtra("DRAWING",model.getJsonStringForModel());
        setResult(Activity.RESULT_OK,returnedIntent);
        finish();
    }

    @Override
    public void onBackPressed() {
        setResult(Activity.RESULT_CANCELED);
        finish();
       // super.onBackPressed();
    }
}

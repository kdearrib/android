package com.apps.cyberleaf.communitydrawing.drawingfragment.fragment.drawingzone;

import com.apps.cyberleaf.communitydrawing.drawingfragment.model.Point;
/**
 * Created by be5nx2 on 13/03/2017.
 */

public interface DrawingZoneListener {

    /**
     * notifie d un nouveau contact avec la zone de dessin
     * @param p correspond au point ou on a touche la zone
     */
    public void onNewTouchInDrawingZone(Point p);

    /**
     * nofitie un mouvement du doigt sans le lever
     * @param p point ou le doigt a ete deplacer
     */
    public void onMoveOnDrawingZone(Point p);

    /**
     * indique qu on a lever le doigt de l ecran
     */
    public void onUpDrawingZone();
}
